const nombre = document.getElementById("link");
let usuario = sessionStorage.getItem("usuario logeado");
const listaItems = document.getElementById('listaItems');

let arrayItems = [];

if(usuario === null){
	usuario = "";
}

nombre.innerHTML = '<a id="n">' + usuario + '</a>';


const guardarDB = () => {
	localStorage.setItem("producto", JSON.stringify(arrayItems));
	PintarDB();
}


const PintarDB = () => {
	listaItems.innerHTML = "";

	arrayItems = JSON.parse(localStorage.getItem("producto"));

	if(arrayItems === null){
		arrayItems = [];
	}else{
		arrayItems.forEach(element => {
			listaItems.innerHTML += `<div class="item1">
			<img src=${element.link} alt="Nintendo">
		</div>
		<div class="derecha1">
			<a href="">${element.nombre}</a>
			<br><br>
			<button class="btnEditar1">Editar</button>
			<br><br><br><br><br>
			<button class="btnEliminar1">Eliminar</button>
		</div>`
		})
	}
}

const Eliminar = (actividad) => {
	let indexArray;
	arrayItems.forEach((element, index) => {

		if(element.nombre === actividad){
			indexArray = index;
		}
	})

	arrayItems.splice(indexArray, 1);
	guardarDB();
}

const Editar = (actividad) => {
	let indexArray = arrayItems.findIndex((element)=>{
		return element.nombre === actividad

		console.log(arrayItems[indexArray]);
	})
}

	
document.addEventListener('DOMContentLoaded', PintarDB);

listaItems.addEventListener("click", (e) => {
	e.preventDefault();

	if(e.target.innerHTML === "Editar" || e.target.innerHTML === "Eliminar"){

		let texto = e.path[1].childNodes[1].innerHTML
		if(e.target.innerHTML === "Eliminar"){
			Eliminar(texto);
		}
		if(e.target.innerHTML === "Editar"){
			Editar(texto);
		}
	}


})
